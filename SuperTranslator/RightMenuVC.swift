//
//  RightMenuVC.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/14/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit
import Localize_Swift

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int)
}


class RightMenuVC: UIViewController {

   
    
    @IBOutlet weak var leftMenuTable: UITableView!
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
   
    var menuOptionsArray:[String]?
    
    var cellImages:[String] = ["settings.png","recommend.png","unlock.png"]
    
    let selectedLangCode:String? = Localize.currentLanguage()
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftMenuTable.delegate = self
        leftMenuTable.dataSource = self
        leftMenuTable.backgroundColor = UIColor.init(rgb: 0x1d063b)
        self.leftMenuTable.separatorStyle = UITableViewCellSeparatorStyle.none
        self.leftMenuTable.separatorColor = UIColor.clear;

        if selectedLangCode == "ar"{
            self.view.semanticContentAttribute = .forceRightToLeft
            self.leftMenuTable.semanticContentAttribute = .forceRightToLeft
            
        }
        else
        {
           self.view.semanticContentAttribute = .forceLeftToRight
            self.leftMenuTable.semanticContentAttribute = .forceLeftToRight
            
        }
        
        
        self.setText()
        
         NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        // Do any additional setup after loading the view.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
        
       menuOptionsArray = ["menu_setting".localized(),"menu_recommandation".localized(),"menu_netwrokUnlock".localized()]
       leftMenuTable.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = button.tag
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RightMenuVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptionsArray!.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath as IndexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.text = menuOptionsArray?[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell.textLabel?.backgroundColor = UIColor.init(rgb:
            0x1d063b)
        if selectedLangCode == "ar"{
            cell.textLabel?.textAlignment = .right
        }else{
            cell.textLabel?.textAlignment = .left
        }
        cell.imageView?.image = UIImage(named:self.cellImages[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.contentView.backgroundColor = UIColor.init(rgb: 0x1d063b)
      
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       
        tableView.separatorColor = UIColor.clear
        
    }
  
    
}

