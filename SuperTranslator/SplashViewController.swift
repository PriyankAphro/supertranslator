//
//  SplashViewController.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/30/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var imageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.

            var imageArray = [CGImage]()
            let imageCount: Int = 3
            for i in 1...imageCount {
                imageArray.append((UIImage(named: String(format:"image\(i)"))?.cgImage!)!)
            }
        
           let animation = CAKeyframeAnimation(keyPath: "contents")
            animation.calculationMode = kCAAnimationLinear
            // Make sure this is kCAAnimationLinear or kCAAnimationCubic other mode doesnt consider the timing function
            animation.duration = CFTimeInterval(floatLiteral: 3.5)
            animation.values = imageArray
            animation.repeatCount = 1
            // Change it for repetition
            animation.isRemovedOnCompletion = false
            animation.fillMode = kCAFillModeForwards
            // To keep the last frame when animation ends
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            imageView.layer.add(animation, forKey: "animation")
        
        
         _ = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(displayMainScreen), userInfo: nil, repeats: false)
    }

    @objc func displayMainScreen() {
        // Something after a delay
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! ViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
