//
//  AppLangViewController.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/20/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit
import Localize_Swift


class AppLangViewController: UIViewController {

    @IBOutlet weak var appLangTable: UITableView!
    
    var langArray: [[String: Any]] = [["langCode": "en", "langName": "setting_lang_eng".localized()], ["langCode": "zh-Hans", "langName": "setting_lang_chin".localized()],["langCode": "fr", "langName": "setting_lang_frc".localized()],["langCode": "de", "langName": "setting_lang_ger".localized()],["langCode": "es", "langName": "setting_lang_spn".localized()]]
    
    //,["langCode": "ar", "langName": "setting_lang_arb".localized()]
 
  var lastSelection: IndexPath!
    
    var selectedLangCode:String? = Localize.currentLanguage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let closeBtn = UIBarButtonItem(image:UIImage(named:"icon_close"), style:.plain, target:self, action:#selector(AppLangViewController.back))
        closeBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = closeBtn
        
        self.title = "menu_setting".localized()
        self.appLangTable.backgroundColor = UIColor.init(rgb: 0x1d063b)
        self.appLangTable.tableFooterView = UIView()
        
        var colors = [UIColor]()
        colors.append(UIColor.init(rgb: 0xe35462))
        colors.append(UIColor.init(rgb: 0xed6a53))
        colors.append(UIColor.init(rgb: 0xff8e3d))
    navigationController?.navigationBar.setGradientBackground(colors: colors)
        // Do any additional setup after loading the view.
      
        if selectedLangCode == "ar"{
            appLangTable.semanticContentAttribute = .forceRightToLeft
        }else{
            appLangTable.semanticContentAttribute = .forceLeftToRight
        }
    }

    @objc func back(){
        
        if self.presentingViewController != nil{
            self.dismiss(animated: true, completion: nil)
        }else{
           self.navigationController?.popViewController(animated: true)
        }

    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension AppLangViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return langArray.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "appLangCell", for: indexPath as IndexPath)
        
        if selectedLangCode == "ar"{
           cell.textLabel?.textAlignment = .right
        }else{
            cell.textLabel?.textAlignment = .left
        }
        cell.textLabel?.text = langArray[indexPath.row]["langName"] as? String
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.backgroundColor = UIColor.init(rgb: 0x1d063b)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
       
        if selectedLangCode == langArray[indexPath.row]["langCode"] as? String{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        tableView.separatorStyle =  UITableViewCellSeparatorStyle.singleLine

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
      
        selectedLangCode = (langArray[indexPath.row]["langCode"] as! String)
       // tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
        self.updateLangugaeArray()
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        cell.contentView.backgroundColor = UIColor.init(rgb: 0x1d063b)
        cell.tintColor = UIColor.white
        cell.backgroundColor =  UIColor.init(rgb: 0x1d063b)
        tableView.separatorColor = UIColor.white.withAlphaComponent(0.2)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
    }
    
    func updateLangugaeArray(){
       
        Localize.setCurrentLanguage(selectedLangCode!)
       
        self.title = "menu_setting".localized()

        if selectedLangCode == "ar"{
            appLangTable.semanticContentAttribute = .forceRightToLeft
        }else{

            appLangTable.semanticContentAttribute = .forceLeftToRight
        }
        self.langArray = [["langCode": "en", "langName": "setting_lang_eng".localized()], ["langCode": "zh-Hans", "langName": "setting_lang_chin".localized()],["langCode": "fr", "langName": "setting_lang_frc".localized()],["langCode": "de", "langName": "setting_lang_ger".localized()],["langCode": "es", "langName": "setting_lang_spn".localized()]]
        
        //,["langCode": "ar", "langName": "setting_lang_arb".localized()]
    }
    
}
