//
//  STWebViewController.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/17/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit
import WebKit



class STWebViewController: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    var urlString: String?
    var viewTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        var colors = [UIColor]()
        colors.append(UIColor.init(rgb: 0xe35462))
        colors.append(UIColor.init(rgb: 0xed6a53))
        colors.append(UIColor.init(rgb: 0xff8e3d))
        navigationController?.navigationBar.setGradientBackground(colors: colors)
        
        let url = URL (string: urlString!)
        let requestObj = URLRequest(url: url!)
        self.webView.load(requestObj)
        self.webView.navigationDelegate = self 

        activityIndicator.startAnimating()
        
        let closeBtn = UIBarButtonItem(image:UIImage(named:"icon_close"), style:.plain, target:self, action:#selector(LanguageListVC.back))
        closeBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = closeBtn
        
        self.title = viewTitle!
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func back(){
        
        if self.presentingViewController != nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
   
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()

    }

}
