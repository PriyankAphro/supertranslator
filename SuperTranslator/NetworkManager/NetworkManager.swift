//
//  NetworkManager.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/11/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import Foundation

final class Netwrokmanager{
  
    static let sharedInstance = Netwrokmanager()
    private init() {
        print("MyClass Initialized")
    }
 
    func converText(fromLang:String, toLang:String, inputText:String, completion: @escaping (String) -> ()) {
     
        let session = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: OperationQueue.main)
        
        var urlComponents = URLComponents(string:"https://translate.googleapis.com/translate_a/single?")!

         urlComponents.queryItems = [
            URLQueryItem(name: "client", value: String("gtx")),
            URLQueryItem(name: "dt", value: String("t")),
            URLQueryItem(name: "ie", value: String("UTF-8")),
            URLQueryItem(name: "oe", value: String("UTF-8")),
            URLQueryItem(name: "sl", value: fromLang),
            URLQueryItem(name: "tl", value: toLang),
            URLQueryItem(name: "q", value: inputText)
        ]
     
        let task = session.dataTask(with: urlComponents.url!, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            do {
                if let data = data{
                    let textArray = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject]
                    print(textArray)
                    let array = textArray[0] as! [AnyObject]
                    let convertedArray = array[0] as! [AnyObject]
                    completion(convertedArray[0] as! String)

                }
            } catch {
                print("Error deserializing JSON: \(error)")
            }
         
        })
        task.resume()

    }

    
}
