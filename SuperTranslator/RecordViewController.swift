//
//  RecordViewController.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/20/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit

protocol RecordViewDelegate : class {
    func didFinishRecording(_ recorderViewController: RecordViewController)
    func didTapStartRecording()
}


class RecordViewController: UIViewController {
  
    open weak var delegate: RecordViewDelegate?
    @IBOutlet weak var recordingLabel:UILabel!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.finishButton.titleLabel?.textColor = UIColor.init(rgb: 0x1d063b)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissView (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
       self.recordingLabel.text =  "text_tapToSpeak".localized()
       self.finishButton.setTitle("text_finish".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }

    @IBAction func voiceConvertClicked(_ sender: Any) {
        
      self.recordingLabel.text = "text_saySomething".localized()
      self.delegate?.didTapStartRecording()
   
    }
    
    @IBAction func stopRecordingClicked(_ sender: Any) {
       
        self.delegate?.didFinishRecording(self)
        dismiss(animated: true, completion: nil)
        
    }
    
    @objc func dismissView (_ sender: UITapGestureRecognizer) {
        
        self.delegate?.didFinishRecording(self)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
