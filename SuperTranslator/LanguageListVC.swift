//
//  LanguageListVC.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/13/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit
import Localize_Swift


class LanguageListVC: UIViewController {
    
    var marrCountryList = [String]()
    var marrFilteredCountryList = [String]()
  
    
    var isFromLang: Bool = false
    var searchActive : Bool = false
    var delegate: LanguageListDelegate?
    var prefLangName: String?
    var prefLangCode: String?
    var langFileName: String?
    
    var viewTitle: String?
    
    var langArray: [AnyObject] = []
    var filteredLangArray: [AnyObject] = []
    var selectedLangCode:String? = Localize.currentLanguage()
    
    @IBOutlet weak var languageSearchBar:UISearchBar!
    @IBOutlet weak var languageTable:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languageSearchBar.placeholder = "text_search".localized()
        
        if selectedLangCode == "ar"{
            languageSearchBar.semanticContentAttribute = .forceRightToLeft
        }else{
            languageSearchBar.semanticContentAttribute = .forceLeftToRight
        }
        
        var colors = [UIColor]()
        colors.append(UIColor.init(rgb: 0xe35462))
        colors.append(UIColor.init(rgb: 0xed6a53))
        colors.append(UIColor.init(rgb: 0xff8e3d))
        navigationController?.navigationBar.setGradientBackground(colors: colors)
     
        self.languageTable.tableFooterView = UIView()

        if let fileUrl = Bundle.main.url(forResource: langFileName, withExtension: "plist"),
            let data = try? Data(contentsOf: fileUrl) {
            if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [AnyObject] {
                langArray = result!
                print(result as Any)
            }
        }

        let closeBtn = UIBarButtonItem(image:UIImage(named:"icon_close"), style:.plain, target:self, action:#selector(LanguageListVC.back))
        closeBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = closeBtn
        
        self.title = viewTitle!
        
        self.languageTable.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LanguageListVC: UITableViewDelegate,UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate {
  
  
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if searchActive{
            return 1
        }else{
            return langArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40))
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = headerView.bounds
        gradient.colors = [UIColor.init(rgb: 0xe35462).cgColor, UIColor.init(rgb: 0xed6a53).cgColor,UIColor.init(rgb: 0xff8e3d).cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        headerView.layer.insertSublayer(gradient, at: 0)

        let headerLabel = UILabel(frame: CGRect(x: 15, y: 8, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        headerLabel.textColor = UIColor.white
         if selectedLangCode == "ar"{
          headerLabel.textAlignment = .right
        }else{
          headerLabel.textAlignment = .left

        }
         headerLabel.text = self.tableView(self.languageTable, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
   
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
      
        if searchActive{
            return "header_Results".localized()
        }else{
            switch(section) {
            case 1:return "header_other".localized()
            default :return "header_popular".localized()
            }
        }
        
    }
    func tableView( _ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
       let cellCountry = self.languageTable.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath)
        
        if(searchActive){
            let array = filteredLangArray
            let dict = array[indexPath.row] as! [String:String]
            cellCountry.textLabel?.text = dict["langName"]
        } else {
             let array = langArray[indexPath.section] as! NSMutableArray
            let dict = array[indexPath.row] as! [String:String]
            cellCountry.textLabel?.text = dict["langName"]
        }
    
        return cellCountry
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let dict:[String:String]!
        if(searchActive){
            let array = filteredLangArray
            dict = array[indexPath.row] as! [String:String]
        } else {
            let array = langArray[indexPath.section] as! NSMutableArray
            dict = array[indexPath.row] as! [String:String]
        }
        prefLangName = dict["langName"]
        prefLangCode = dict["langCode"]

        self.delegate?.updatePreferredLanguage(prefLangName,prefLangCode)
        
        if self.presentingViewController != nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
      if(searchActive) {
            return filteredLangArray.count
      }else{
        
        switch section {
        case 0:
            let array = langArray[section]
            return array.count
        default:
            let array = langArray[section]
            return array.count
        }
        
        }
      
 
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Put your key in predicate that is "Name"
        let searchPredicate = NSPredicate(format: "langName contains[cd] %@", searchText)
       
        var array1 = langArray[0] as! [AnyObject]
        let array2 = langArray[1] as! [AnyObject]
        array1 += array2

        let mutableArray : NSMutableArray = NSMutableArray(array: array1)
        filteredLangArray = mutableArray.filtered(using: searchPredicate) as [AnyObject]
        
        print ("array = \(filteredLangArray)")
        searchActive = !filteredLangArray.isEmpty
        
        self.languageTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
       searchBar.setValue("text_cancel".localized(), forKey:"_cancelButtonText")
        self.languageSearchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        searchActive = false;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
   @objc func back(){
        self.delegate?.updatePreferredLanguage(prefLangName,prefLangCode)
    
        if self.presentingViewController != nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
protocol LanguageListDelegate: class {
  
    func updatePreferredLanguage(_ langName: String?, _ langCode: String?)

}


