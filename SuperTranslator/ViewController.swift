//
//  ViewController.swift
//  SuperTranslator
//
//  Created by Priyank Maheshwari on 11/11/17.
//  Copyright © 2017 aphroecs. All rights reserved.
//

import UIKit
import Vision
import Speech
import AVFoundation
import TesseractOCR
import Localize_Swift
import GoogleMobileAds
import Foundation
import SystemConfiguration

class ViewController: UIViewController,LanguageListDelegate,SlideMenuDelegate, UINavigationControllerDelegate, SFSpeechRecognizerDelegate,RecordViewDelegate,UITextViewDelegate {
    
    @IBOutlet weak var convertBtn: UIButton!
    @IBOutlet weak var fromBtn: UIButton!
    @IBOutlet weak var toBtn: UIButton!
    @IBOutlet weak var swipeBtn: UIButton!
   
    @IBOutlet weak var microphoneButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
   
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var toLbl: UILabel!
    @IBOutlet weak var offlineLbl: UILabel!

    @IBOutlet weak var inputHolderView: UIView!
    @IBOutlet weak var outputHolderView: UIView!
    @IBOutlet weak var addHolderView: UIView!

    @IBOutlet weak var inputTextbox: UITextView!
    @IBOutlet weak var outPutTextbox: UITextView!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var appNameLbl:UILabel?
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = "ca-app-pub-6901539703728543/6070213479"
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        return adBannerView
    }()
    
    var interstitial: GADInterstitial?
    
   // var model: VNCoreMLModel!
   // var textMetadata = [Int: [Int: String]]()

    var isFromLang: Bool = false
    var fromLangCode: String?
    var toLangCode: String?
    var isAnimated: Bool = false
    
    var recorderView: RecordViewController!
    let serviceManager = Netwrokmanager.sharedInstance
   
    /*Text To Speech*/
    let synth = AVSpeechSynthesizer()
    var myUtterance = AVSpeechUtterance(string: "")
  

    /*Speach To Text*/
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.current)
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()

    var selectedLangCode:String? = Localize.currentLanguage()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.view.backgroundColor = UIColor.init(rgb: 0x1d063b)
        activityIndicator.hidesWhenStopped = true
        self.navigationController?.navigationBar.isHidden = false

        self.offlineLbl.isHidden = true
        
        let isConnected = self.isInternetAvailable()
        if !isConnected{
            // toast with a specific duration and position
            self.view.makeToast("error_offline".localized(), duration: 10.0, position: .bottom)
        }
       // loadModel()

        self.addSlideMenuButton()
        self.setText()
        self.showAndHideViews(isHide: true)
        
        var colors = [UIColor]()
        colors.append(UIColor.init(rgb: 0xe35462))
        colors.append(UIColor.init(rgb: 0xed6a53))
        colors.append(UIColor.init(rgb: 0xff8e3d))
        navigationController?.navigationBar.setGradientBackground(colors: colors)
       
        fromLangCode = "auto"
        toLangCode = "es"
      
        fromBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        fromBtn.setTitle("Auto Detect", for: .normal)
        fromBtn.titleLabel?.textAlignment = .center
       
        toBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        toBtn.setTitle("Spanish", for: .normal)
        toBtn.titleLabel?.textAlignment = .center

        
        if self.fromBtn.titleLabel?.text == "Auto Detect"{
            self.swipeBtn.alpha = 0.5
            self.swipeBtn.isEnabled = false
        }else{
            self.swipeBtn.alpha = 1.0
            self.swipeBtn.isEnabled = true
        }
        speechRecognizer?.delegate = self  //3
       
        SFSpeechRecognizer.requestAuthorization { (authStatus) in  //4
            
            var isButtonEnabled = false
            
            switch authStatus {  //5
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.microphoneButton.isEnabled = isButtonEnabled
            }
        }
        
     NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
      adBannerView.load(GADRequest())
        
      interstitial = createAndLoadInterstitial()

    }
   
   
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
  
    func showAndHideViews(isHide : Bool){
        if isHide{
            self.outPutTextbox.isHidden = true
            self.outputHolderView.isHidden = true
            self.speakerButton.isHidden = true
            self.shareButton.isHidden = true
            self.isAnimated =   false
            
            self.inputHolderView.frame =  CGRect(x:self.inputHolderView.frame.origin.x, y: self.fromLbl.frame.origin.y+self.fromLbl.frame.size.height+10, width:self.inputHolderView.frame.size.width, height:self.inputHolderView.frame.size.height)
            
            self.inputTextbox.frame = CGRect(x:self.inputHolderView.frame.origin.x, y: self.fromLbl.frame.origin.y+self.fromLbl.frame.size.height+10, width:self.inputTextbox.frame.size.width, height:self.inputTextbox.frame.size.height)
          
            self.microphoneButton.frame = CGRect(x:self.microphoneButton.frame.origin.x, y: self.inputHolderView.frame.origin.y+self.inputHolderView.frame.size.height+15, width:self.microphoneButton.frame.size.width, height:self.microphoneButton.frame.size.height)
            
            self.cameraButton.frame = CGRect(x:self.cameraButton.frame.origin.x, y: self.inputHolderView.frame.origin.y+self.inputHolderView.frame.size.height+15, width:self.cameraButton.frame.size.width, height:self.cameraButton.frame.size.height)
            
            self.convertBtn.frame = CGRect(x:self.convertBtn.frame.origin.x, y: self.inputHolderView.frame.origin.y+self.inputHolderView.frame.size.height-self.convertBtn.frame.size.height-5, width:self.convertBtn.frame.size.width, height:self.convertBtn.frame.size.height)
            
        }else{
            
            guard !isAnimated else{
                return
            }
            self.isAnimated =   true
            self.outPutTextbox.isHidden = false
            self.outputHolderView.isHidden = false
            self.speakerButton.isHidden = false
            self.shareButton.isHidden = false
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [], animations: {
               
                self.inputHolderView.frame =  CGRect(x:self.inputHolderView.frame.origin.x, y: self.inputHolderView.frame.origin.y, width:self.outputHolderView.frame.size.width, height:self.outputHolderView.frame.size.height)
                
                self.inputTextbox.frame = CGRect(x:self.inputHolderView.frame.origin.x, y: self.inputHolderView.frame.origin.y, width:self.outPutTextbox.frame.size.width, height:self.outPutTextbox.frame.size.height)
                
                self.microphoneButton.frame = CGRect(x:self.inputHolderView.frame.size.width-self.microphoneButton.frame.size.width, y: self.inputHolderView.frame.origin.y+15, width:self.microphoneButton.frame.size.width, height:self.microphoneButton.frame.size.height)
                
                self.cameraButton.frame = CGRect(x:self.inputHolderView.frame.size.width-self.microphoneButton.frame.size.width, y: self.inputHolderView.frame.origin.y+self.microphoneButton.frame.size.height+35, width:self.cameraButton.frame.size.width, height:self.cameraButton.frame.size.height)
                
                self.convertBtn.frame = CGRect(x:self.view.frame.size.width/2-self.convertBtn.frame.size.width/2, y: self.inputHolderView.frame.origin.y+self.outPutTextbox.frame.size.height+15, width:self.convertBtn.frame.size.width, height:self.convertBtn.frame.size.height)
           
                self.outputHolderView.frame =  CGRect(x:self.outputHolderView.frame.origin.x, y: self.convertBtn.frame.origin.y+self.convertBtn.frame.size.height+10, width:self.outputHolderView.frame.size.width, height:self.outputHolderView.frame.size.height)
                
                self.outPutTextbox.frame = CGRect(x:self.outPutTextbox.frame.origin.x, y: self.convertBtn.frame.origin.y+self.convertBtn.frame.size.height+10, width:self.outPutTextbox.frame.size.width, height:self.outPutTextbox.frame.size.height)
                
                self.speakerButton.frame = CGRect(x:self.outputHolderView.frame.size.width-self.speakerButton.frame.size.width, y: self.outputHolderView.frame.origin.y+15, width:self.speakerButton.frame.size.width, height:self.speakerButton.frame.size.height)
                
                self.shareButton.frame = CGRect(x:self.outputHolderView.frame.size.width-self.shareButton.frame.size.width, y: self.outputHolderView.frame.origin.y+self.shareButton.frame.size.height+35, width:self.shareButton.frame.size.width, height:self.shareButton.frame.size.height)
            
            
            }, completion: { (finished: Bool) in
            
            })
            
        }
    }
    
    @objc func setText(){
      
        inputTextbox.text = "input_placeholder".localized()
        inputTextbox.textColor = UIColor.lightGray
        fromLbl.text = "select_fromLang".localized()
        toLbl.text = "select_toLang".localized()
        self.outPutTextbox.text = ""
        
        self.selectedLangCode =  Localize.currentLanguage()
        if selectedLangCode == "ar"{
          //  UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            self.inputTextbox.textAlignment = .right
            self.outPutTextbox.textAlignment = .right
           // appNameLbl?.textAlignment = NSTextAlignment.right
        }else{
          //  UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            self.inputTextbox.textAlignment = .left
            self.outPutTextbox.textAlignment = .left
           // appNameLbl?.textAlignment = NSTextAlignment.left
        }
        
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
    
        self.view.endEditing(true)
   
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
   
        if (inputTextbox?.text == "input_placeholder".localized())
       {
        inputTextbox!.text = nil
        inputTextbox!.textColor = UIColor.white
       }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
      
        if inputTextbox!.text.isEmpty
        {
            inputTextbox!.text = "input_placeholder".localized()
            inputTextbox!.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }
    
    @IBAction func swipeLanguage(_ sender: Any) {
       
        let fromLang = self.fromBtn.titleLabel?.text
        let toLang =  self.toBtn.titleLabel?.text
      
        let fromCode = self.fromLangCode
        let toCode = self.toLangCode
       
        UIView.animate(withDuration:0.2, animations: { () -> Void in
            if self.swipeBtn.transform == .identity {
                self.swipeBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 0.999))
            } else {
                self.swipeBtn.transform = .identity
            }
       
        })
        self.fromBtn.setTitle(toLang, for: .normal)
        self.fromLangCode = toCode
        
        self.toBtn.setTitle(fromLang, for: .normal)
        self.toLangCode = fromCode

    }
    
    @IBAction func convertInputText(_ sender: Any) {
        self.inputTextbox.resignFirstResponder()
       
        let isConnected = self.isInternetAvailable()
        if !isConnected{
            self.view.makeToast("error_offline".localized(), duration: 5.0, position: .bottom)
            return
        }
        
        guard let text = inputTextbox.text, !text.isEmpty ,inputTextbox.text !=  "input_placeholder".localized() else {
            // basic usage
            self.view.makeToast("error_emptyText".localized())
            return
        }
        activityIndicator.startAnimating()

        serviceManager.converText(fromLang: fromLangCode!, toLang: toLangCode!, inputText: inputTextbox.text) { (Result) in
            DispatchQueue.main.async {
                self.showAndHideViews(isHide: false)
                self.activityIndicator.stopAnimating()
                self.outPutTextbox.text = Result
            }
        }
     
        _ = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(displayAd), userInfo: nil, repeats: false)
        
    }
    
    @objc func displayAd() {
        // Something after a delay
        
        if (interstitial?.isReady)! {
            interstitial?.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    @IBAction func voiceConvertClicked(_ sender: Any) {
        
        self.inputTextbox.text = ""
        self.outPutTextbox.text = ""
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        recorderView = storyboard.instantiateViewController(withIdentifier: "RecordViewController") as! RecordViewController
        recorderView.delegate = self
        recorderView.modalTransitionStyle = .crossDissolve
        recorderView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(recorderView, animated: false, completion: nil)

    }
    func didFinishRecording(_ recorderViewController: RecordViewController) {
         self.stopListening()
    }
    
    func didTapStartRecording() {
 
        if audioEngine.isRunning{
            self.audioEngine.stop()
            self.recognitionRequest?.endAudio()
            self.recognitionTask?.cancel()
            self.recognitionTask = nil
        }else{
            startRecording()
        }
    }
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
       
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            if result != nil {
                self.inputTextbox.text = result?.bestTranscription.formattedString  //9
                isFinal = (result?.isFinal)!
            }

            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)

                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.microphoneButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
    }
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            microphoneButton.isEnabled = true
        } else {
            microphoneButton.isEnabled = false
        }
    }
    
    func stopListening(){
     if audioEngine.isRunning{
      self.audioEngine.stop()
      self.recognitionRequest?.endAudio()
      self.recognitionTask?.cancel()
      self.recognitionTask = nil
       }
        
    }
    
    @IBAction func speakTextClicked(_ sender: Any) {
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setMode(AVAudioSessionModeDefault)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        myUtterance = AVSpeechUtterance(string: outPutTextbox.text)
        myUtterance.rate = 0.3
        myUtterance.voice = AVSpeechSynthesisVoice(language: toLangCode)

        synth.speak(myUtterance)
        
    }
    @IBAction func shareConvertedText(_ sender: Any) {
        // 1
        if outPutTextbox.text.isEmpty {
            return
        }
        // 2
        let activityViewController = UIActivityViewController(activityItems:
            [outPutTextbox.text], applicationActivities: nil)
        // 3
        let excludeActivities:[UIActivityType] = [
            .assignToContact,
            .saveToCameraRoll,
            .addToReadingList,
            .postToFlickr,
            .postToVimeo]
        activityViewController.excludedActivityTypes = excludeActivities
        // 4
        present(activityViewController, animated: true)
        
    }
    
    @IBAction func fromLanguageClicked(_ sender: Any){
        
        isFromLang = true
        let languageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LanguageListVC") as! LanguageListVC
        languageVC.isFromLang = true
        languageVC.viewTitle = "title_fromLanguage".localized()
        languageVC.delegate = self
        languageVC.langFileName = "STFromLanguage"
        let navigationControlr = UINavigationController(rootViewController: languageVC)
        self.present(navigationControlr, animated: true, completion: nil)
        
    }
    
   
   
    @IBAction func ToLanguageClicked(_ sender: Any){
        isFromLang = false
        let languageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LanguageListVC") as! LanguageListVC
        languageVC.delegate = self
        languageVC.viewTitle = "title_toLanguage".localized()
        languageVC.langFileName = "STToLanguage"
        let navigationControlr = UINavigationController(rootViewController: languageVC)
        self.present(navigationControlr, animated: true, completion: nil)
        
    }
    func updatePreferredLanguage(_ langName: String?, _ langCode: String?) {
      
        if isFromLang {
            guard langName != nil else {
                return
            }
            self.fromLangCode = langCode
            self.fromBtn.setTitle(langName, for: .normal)
            
        }else{
            guard langName != nil else {
                return
            }
             self.toLangCode = langCode
            self.toBtn.setTitle(langName, for: .normal)

        }
      
        if self.fromBtn.titleLabel?.text == "Auto Detect"{
            self.swipeBtn.alpha = 0.5
            self.swipeBtn.isEnabled = false
        }else{
            self.swipeBtn.alpha = 1.0
            self.swipeBtn.isEnabled = true
        }
    }
    
    func addSlideMenuButton( ){
      
        let btnShowMenu = UIButton(type: UIButtonType.custom)
        btnShowMenu.setBackgroundImage(UIImage(named: "more"), for: UIControlState.normal)
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.rightBarButtonItem = customBarItem;
        
        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 300.0, height: 44.0))
        customView.backgroundColor = UIColor.clear
       
        self.appNameLbl = UILabel(frame: CGRect(x: 10, y: 0.0, width: 300.0, height: 44.0))
        self.appNameLbl?.text = "Super Translator"
        self.appNameLbl?.textColor = UIColor.white
        self.appNameLbl?.font = UIFont.systemFont(ofSize: 20
        )
        appNameLbl?.textAlignment = NSTextAlignment.left

//        if selectedLangCode == "ar"{
//            appNameLbl?.textAlignment = NSTextAlignment.right
//        }else{
//            appNameLbl?.textAlignment = NSTextAlignment.left
//        }
        appNameLbl?.backgroundColor = UIColor.clear
        customView.addSubview(appNameLbl!)
        
        let leftButton = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftButton
        
    }
    
    func defaultMenuImage() -> UIImage {
        let defaultMenuImage = UIImage(named:"more")
        return defaultMenuImage!;
    }
    
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            //  self.slideMenuItemSelectedAtIndex(-1);
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
             
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = 1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
          
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : RightMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "RightMenuVC") as! RightMenuVC
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()

        menuVC.view.frame=CGRect(x:  UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
  
    func slideMenuItemSelectedAtIndex(_ index: Int) {
        
        if index != -1{
           let webViewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "STWebViewController") as! STWebViewController
            switch index{
            case 1:
                webViewVC.viewTitle = "menu_recommandation".localized()
                webViewVC.urlString  = "https://www.unlockninja.com/top_apps?utm_source=supertranslator&utm_medium=iOS"
                let navigationControlr = UINavigationController(rootViewController: webViewVC)
                self.present(navigationControlr, animated: true, completion: nil)
            case 2:
                webViewVC.viewTitle = "menu_netwrokUnlock".localized()
                webViewVC.urlString  = "https://www.unlockninja.com?utm_source=supertranslator&utm_medium=iOS"
                let navigationControlr = UINavigationController(rootViewController: webViewVC)
                self.present(navigationControlr, animated: true, completion: nil)
            default:
                let appLangVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppLangViewController") as! AppLangViewController
                let navigationControlr = UINavigationController(rootViewController: appLangVC)
                self.present(navigationControlr, animated: true, completion: nil)
            }
           
        }
      
    }
    
//    private func loadModel() {
//        model = try? VNCoreMLModel(for: Alphanum_28x28().model)
//    }
//
    // MARK: IBAction
    
    @IBAction func pickImageClicked(_ sender: UIButton) {
        view.endEditing(true)
        presentImagePicker()
    }
    
    // Tesseract Image Recognition
    func performImageRecognition(_ image: UIImage) {
        
        if let tesseract = G8Tesseract(language: "eng+fra") {
            tesseract.engineMode = .tesseractCubeCombined
            tesseract.pageSegmentationMode = .auto
            tesseract.image = image.g8_blackAndWhite()
            tesseract.recognize()
            let formatedText = tesseract.recognizedText.replacingOccurrences(of: "\n", with: " ")
            
            guard !formatedText.isEmpty, formatedText != " " else {
                // basic usage
                self.view.makeToast("error_imageRead".localized())
                activityIndicator.stopAnimating()
                self.inputTextbox.text = ""
                return
            }
            self.inputTextbox.text = formatedText
            self.inputTextbox!.textColor = UIColor.white

        }
        activityIndicator.stopAnimating()
        convertBtn.isHidden = false

    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    
    
}
// MARK: - AdMob Implementation

extension ViewController: GADBannerViewDelegate,GADInterstitialDelegate{
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        bannerView.autoresizingMask = .flexibleWidth
        self.addHolderView.addSubview(bannerView)
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-6901539703728543/9821702322")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("Interstitial loaded successfully")
      //  ad.present(fromRootViewController: self)
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        print("Fail to receive interstitial")
    }
  
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}


// MARK: - UIImagePickerControllerDelegate
extension ViewController: UIImagePickerControllerDelegate {
    func presentImagePicker() {
        
        let imagePickerActionSheet = UIAlertController(title:nil,
                                                       message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(title: "text_takePhoto".localized(),
                                             style: .default) { (alert) -> Void in
                                                let imagePicker = UIImagePickerController()
                                                imagePicker.delegate = self
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker, animated: true)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(title: "text_chooseExisting".localized(),
                                          style: .default) { (alert) -> Void in
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = self
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker, animated: true)
        }
        imagePickerActionSheet.addAction(libraryButton)
        
        let cancelButton = UIAlertAction(title: "text_cancel".localized(), style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
        
        present(imagePickerActionSheet, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let selectedPhoto = info[UIImagePickerControllerOriginalImage] as? UIImage,
            let scaledImage = selectedPhoto.scaleImage(640) {
            
            activityIndicator.startAnimating()
            convertBtn.isHidden = true
            dismiss(animated: true, completion: {
                self.performImageRecognition(scaledImage)
            })
        }
    }
}
// MARK: - UIImage extension
extension UIImage {
    func scaleImage(_ maxDimension: CGFloat) -> UIImage? {
        
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        
        if size.width > size.height {
            let scaleFactor = size.height / size.width
            scaledSize.height = scaledSize.width * scaleFactor
        } else {
            let scaleFactor = size.width / size.height
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: .zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
}


